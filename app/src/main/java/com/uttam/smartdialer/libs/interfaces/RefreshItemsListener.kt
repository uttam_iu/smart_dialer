package com.uttam.smartdialer.libs.interfaces

interface RefreshItemsListener {
    fun refreshItems()
}
