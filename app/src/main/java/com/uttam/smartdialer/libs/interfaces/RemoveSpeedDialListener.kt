package com.uttam.smartdialer.libs.interfaces

interface RemoveSpeedDialListener {
    fun removeSpeedDial(ids: ArrayList<Int>)
}
