package com.uttam.smartdialer.libs.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.uttam.smartdialer.libs.helpers.ACCEPT_CALL
import com.uttam.smartdialer.libs.helpers.CallManager
import com.uttam.smartdialer.libs.helpers.DECLINE_CALL

class CallActionReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        when (intent.action) {
            ACCEPT_CALL -> CallManager.accept()
            DECLINE_CALL -> CallManager.reject()
        }
    }
}
