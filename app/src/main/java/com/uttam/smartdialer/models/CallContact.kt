package com.uttam.smartdialer.models

data class CallContact(var name: String, var photoUri: String, var number: String)
